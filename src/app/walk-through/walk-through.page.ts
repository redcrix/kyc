import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastController, AlertController, } from '@ionic/angular';

import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../api.service';
import { Facebook ,FacebookLoginResponse} from '@ionic-native/facebook/ngx';


@Component({
  selector: 'app-walk-through',
  templateUrl: './walk-through.page.html',
  styleUrls: ['./walk-through.page.scss'],
})
export class WalkThroughPage implements OnInit {

  constructor(public api: RestApiService,
    public loadingController: LoadingController,
 
    public toastController: ToastController,
    public alertController: AlertController, private router: Router ,private fb: Facebook ) {

    if (localStorage.getItem('LoggedInUser_data') != null) {
      this.router.navigate(['MyTab']);  
  } if(localStorage.getItem('LoggedInUser_data') === null) {
      console.log('done');
  }


  this.countdown();

   }

   countdown(){
   let countDownDate = new Date("Jul 31, 2019 14:50:25").getTime();

    // Update the count down every 1 second
    let x = setInterval(function () {

      // Get todays date and time
      let now = new Date().getTime();

      // Find the distance between now and the count down date
      let distance = countDownDate - now;
      // Time calculations for days, hours, minutes and seconds
      let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);
      // console.log(now, "now", "countDownDate", countDownDate, "distance", distance, "days", days);

      // Output the result in an element with id="demo"
      document.getElementById("demo").innerHTML = days + "d " + hours + "h "
        + minutes + "m " + seconds + "s ";

      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
      }
    }, 1000);

  }

  ngOnInit() {
  }

  continue_(){
    this.router.navigate(['LandingPage']);
  }

  start(){
    this.router.navigate(['register']);
  }

  login(){
    this.router.navigate(['login']);
  }

  async socialFb(){

    this.fb.login(['public_profile', 'user_photos', 'email', 'user_birthday'])
    .then( (res: FacebookLoginResponse) => {

        // The connection was successful
        if(res.status == "connected") {

            // Get user ID and Token
            var fb_id = res.authResponse.userID;
            var fb_token = res.authResponse.accessToken;

            // Get user infos from the API
            this.fb.api("/me?fields=name,gender,birthday,email", []).then((user) => {

                // Get the connected user details
                var gender    = user.gender;
                var birthday  = user.birthday;
                var name      = user.name;
                var email     = user.email;

                console.log("=== USER INFOS ===");
                console.log("Gender : " + gender);
                console.log("Birthday : " + birthday);
                console.log("Name : " + name);
                console.log("Email : " + email);

                // => Open user session and redirect to the next page

            });

        } 
        // An error occurred while loging-in
        else {

            console.log("An error occurred...");

        }

    })
    .catch((e) => {
        console.log('Error logging into Facebook', e);
    });
  }

}
